package com.eva.dao.system.dto;

import lombok.Data;

import java.util.List;

/**
 * 查询系统部门参数
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:59
 */
@Data
public class QuerySystemDepartmentDTO {

    private Integer id;

    private List<Integer> ids;
}
